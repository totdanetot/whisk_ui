from mimesis import Person
from pages.SignUpPopup import SignUpPopup
from pages.Header import Header
from pytest_testconfig import config


def test_sign_up(driver):
    driver.get(config['urls']['base_url'])
    person = Person()
    email = person.email()
    driver.find_element_by_xpath(SignUpPopup.EMAIL_OR_PHONE_NUMBER_FLD).send_keys(email)
    driver.find_element_by_xpath(SignUpPopup.CONTINUE_BTN).click()
    assert driver.find_element_by_xpath(Header.AVATAR_BTN)


def test_log_out(driver, sign_up):
    driver.find_element_by_xpath(Header.AVATAR_BTN).click()
    driver.find_element_by_xpath(Header.LOG_OUT_BTN).click()
    assert driver.find_element_by_xpath(Header.LOG_IN_BTN).is_displayed()
