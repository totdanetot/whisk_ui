from pages.ShoppingList import ShoppingList
from pytest_testconfig import config


def test_add_item_to_list(driver, sign_up):
    driver.find_element_by_xpath(ShoppingList.OPEN_ITEMS_LIST_ICON).click()
    driver.find_element_by_xpath(ShoppingList.CHECK_SINGLE_ITEM_ICON).click()
    assert driver.page_source.find(config['labels']['1_item_in_the_list'])


def test_remove_item_from_list(driver, sign_up, add_item_to_list):
    driver.find_element_by_xpath(ShoppingList.CHECK_SINGLE_ITEM_ICON).click()
    assert driver.page_source.find(config['labels']['0_items_in_the_list'])

