### Установка зависимостей:
* `pip3 install -r requirements.txt`

### Для выполнения тестов необходимо чтобы chromedriver:
* был установлен
* прописан в PATH

### Запуск всех тестов в командной строке с применением конфига:
* `pytest --tc-file=test_config.yaml`
