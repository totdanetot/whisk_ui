import pytest
from selenium import webdriver
from mimesis import Person
from pytest_testconfig import config
from pages.SignUpPopup import SignUpPopup
from pages.Header import Header
from pages.ShoppingList import ShoppingList


@pytest.fixture()
def driver(request):
    options = webdriver.ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('headless')
    options.add_argument('start-maximized')
    options.add_argument('disable-infobars')
    options.add_argument('window-size=1920x1080')
    options.add_argument('--verbose')
    options.add_argument('--disable-extensions')
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-dev-shm-usage')
    wd = webdriver.Chrome(options=options)
    wd.implicitly_wait(10)
    request.addfinalizer(wd.quit)
    return wd


@pytest.fixture()
def sign_up(driver):
    driver.get(config['urls']['base_url'])
    person = Person()
    email = person.email()
    driver.find_element_by_xpath(SignUpPopup.EMAIL_OR_PHONE_NUMBER_FLD).send_keys(email)
    driver.find_element_by_xpath(SignUpPopup.CONTINUE_BTN).click()
    assert driver.find_element_by_xpath(Header.AVATAR_BTN)


@pytest.fixture()
def add_item_to_list(driver):
    driver.find_element_by_xpath(ShoppingList.OPEN_ITEMS_LIST_ICON).click()
    driver.find_element_by_xpath(ShoppingList.CHECK_SINGLE_ITEM_ICON).click()
    assert driver.page_source.find(config['labels']['1_item_in_the_list'])